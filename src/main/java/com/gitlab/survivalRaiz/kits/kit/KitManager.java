package com.gitlab.survivalRaiz.kits.kit;

import com.gitlab.survivalRaiz.kits.Kits;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Bridges between the kits config and the kit command to fetch the config data and convert into items,
 * as well as testing whether a player can get the kit or not.
 */
public class KitManager {

    private final Kits plugin;

    public KitManager(Kits plugin) {
        this.plugin = plugin;
    }

    /**
     * Tells whether a player can rescue a kit or not. Tests if the kit's cooldown has passed and if the kit exists.
     *
     * @param player The player trying to get the kit.
     * @param kit    The kit the player is trying to get.
     * @return true if the player can rescue the kit.
     */
    public boolean canRescue(UUID player, String kit) {
        if (!plugin.getKitsConfig().hasPermission(player, kit))
            return false;

        return plugin.getCore().getDbManager().getKitReadyTime(player, kit).isBefore(LocalDateTime.now());
    }

    /**
     * Gives the kit to a player and updates the delay time in the db.
     * @param player The player to be given the kit
     * @param kit The kit to be given
     */
    public void getKit(Player player, String kit) {
        final Set<ItemStack> items = plugin.getKitsConfig().fetchKit(kit);

        items.forEach(itemStack -> player.getInventory().addItem(itemStack));

//        plugin.getCore().getDbManager().setKitReadyDate(player.getUniqueId(), kit,
//                LocalDateTime.now().plusHours(plugin.getKitsConfig().getHours(kit)));
    }
}
