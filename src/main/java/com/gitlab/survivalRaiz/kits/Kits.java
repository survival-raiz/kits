package com.gitlab.survivalRaiz.kits;

import api.skwead.commands.CommandManager;
import api.skwead.commands.ConfigCommand;
import com.gitlab.survivalRaiz.core.Core;
import com.gitlab.survivalRaiz.kits.commands.KitCmd;
import com.gitlab.survivalRaiz.kits.config.KitsConfig;
import com.gitlab.survivalRaiz.kits.kit.KitManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Kits extends JavaPlugin {

    private Core core;

    private KitsConfig kitsConfig;
    private KitManager kitManager;

    @Override
    public void onEnable() {
        setupCore();

        kitsConfig = new KitsConfig(this);
        kitManager = new KitManager(this);

        setupCommands();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    private void setupCommands() {
        final Set<ConfigCommand> cmds = new HashSet<>();

        cmds.add(new KitCmd(this));

        try {
            new CommandManager(this).getConfig().generateFrom(cmds);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean setupCore() {
        if (Bukkit.getPluginManager().getPlugin("Core") == null)
            return false;

        this.core = (Core) Bukkit.getPluginManager().getPlugin("Core");
        return true;
    }

    public Core getCore() {
        return core;
    }

    public KitsConfig getKitsConfig() {
        return kitsConfig;
    }

    public KitManager getKitManager() {
        return kitManager;
    }
}
