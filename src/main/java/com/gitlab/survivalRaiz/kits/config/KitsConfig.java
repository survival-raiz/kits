package com.gitlab.survivalRaiz.kits.config;

import api.skwead.storage.file.yml.YMLConfig;
import com.gitlab.survivalRaiz.kits.item.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.*;

public class KitsConfig extends YMLConfig {

    private final Set<String> kits = new HashSet<>();

    public KitsConfig(JavaPlugin plugin) {
        super("kits", plugin);

        try {
            createConfig(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createConfig(Set<String> set) throws IOException {
        final Set<String> configKits =
                Objects.requireNonNull(getConfig().getConfigurationSection("")).getKeys(false);

        for (String param : configKits) {
            if (super.getConfig().get(param + ".delay") == null) {
                super.getConfig().createSection(param + ".delay");
                super.getConfig().set(param + ".delay", 1);
                super.getConfig().save(super.getFile());
            }

            if (super.getConfig().get(param + ".name") == null) {
                super.getConfig().createSection(param + ".name");
                super.getConfig().set(param + ".name", param);
                super.getConfig().save(super.getFile());
            } else {
                kits.add(super.getConfig().getString(param + ".name"));
            }
        }
    }

    /**
     * Tells if a player has the permission required to get the kit
     *
     * @param player The player to be tested
     * @param kit    The kit the player is trying to get
     * @return true if the permission field for the kit in the config does not exist
     * or if the player has the permission listed there
     */
    public boolean hasPermission(UUID player, String kit) {
        if (super.getConfig().getString(kit + ".permission") == null)
            return true;

        return Bukkit.getPlayer(player).hasPermission(super.getConfig().getString(kit + ".permission"));
    }

    /**
     * Parses the config text into a set of items to be handled.
     *
     * @param kit The kit name
     * @return The items in the config, or null if the kit does not exist
     */
    public Set<ItemStack> fetchKit(String kit) {
        if (!kits.contains(kit))
            return new HashSet<>();

        final Set<ItemStack> items = new HashSet<>();
        final Set<String> configKit =
                Objects.requireNonNull(getConfig().getConfigurationSection(kit + ".items")).getKeys(false);

        configKit.forEach(System.out::println);

        for (String s : configKit) {
            final String base = kit + ".items." + s;
            final Material material = Material.getMaterial(super.getConfig().getString(base + ".material"));
            if (material == null) continue;

            final ItemBuilder builder = new ItemBuilder(material);

            final int amount = super.getConfig().getInt(base + ".amount");
            System.out.println(amount);
            if (amount > 0)
                builder.quantity(amount);

            final String displayName = super.getConfig().getString(base + ".name");
            System.out.println(displayName);
            if (displayName != null)
                builder.name(displayName);

            final List<String> lore = super.getConfig().getStringList(base + ".lore");
            lore.forEach(System.out::println);
            if (!lore.isEmpty())
                builder.lore(lore);

            final Map<Enchantment, Integer> enchantments = new HashMap<>();
            final List<String> rawEnchs = super.getConfig().getStringList(base + ".enchantments");
            rawEnchs.forEach(ln -> enchantments.put(
                    Enchantment.getByName(ln.split(" : ")[0]),
                    Integer.valueOf(ln.split(" : ")[1])));
            if (!enchantments.isEmpty())
                builder.enchant(enchantments);

            items.add(builder.get());
        }

        return items;
    }

    /**
     * Gets the delay time in hours for the kit
     * @param kit The kit whose delay we want to know
     * @return The delay time in hours
     */
    public int getHours(String kit) {
        return super.getConfig().getInt(kit + ".delay");
    }

    public Set<String> getKits() {
        return kits;
    }
}

/*
kitName:
  name: "name"
  permission: "permission"
  delay: <time in hours>
  items:
    a:
      material: "material name"
      name: "display name"
      lore:
        - item
        - lore
      enchantments:
        - item : level
        - enchantments : level
    b:
    //...
*/
