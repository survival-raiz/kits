package com.gitlab.survivalRaiz.kits.item;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import java.util.Map;

public class ItemBuilder {
    private final ItemStack itemStack;

    public ItemBuilder(Material type) {
        this.itemStack = new ItemStack(type);
    }

    public ItemBuilder quantity(int amount) {
        itemStack.setAmount(amount);

        return this;
    }

    public ItemBuilder name(String name) {
        final ItemMeta itemMeta = itemStack.getItemMeta();

        if (itemMeta == null)
            return this;

        itemMeta.setDisplayName(name);

        itemStack.setItemMeta(itemMeta);
        return this;
    }

    public ItemBuilder lore(List<String> lore) {
        final ItemMeta itemMeta = itemStack.getItemMeta();

        if (itemMeta == null)
            return this;

        itemMeta.setLore(lore);

        itemStack.setItemMeta(itemMeta);
        return this;
    }

    public ItemBuilder enchant(Map<Enchantment, Integer> enchantments) {
        final ItemMeta itemMeta = itemStack.getItemMeta();

        if (itemMeta == null)
            return this;

        enchantments.forEach((key, value) -> itemMeta.addEnchant(key, value, true));

        itemStack.setItemMeta(itemMeta);
        return this;
    }

    public ItemStack get() {
        return itemStack;
    }
}
