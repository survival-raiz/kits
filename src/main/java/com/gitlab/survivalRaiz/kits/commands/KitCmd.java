package com.gitlab.survivalRaiz.kits.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import com.gitlab.survivalRaiz.kits.Kits;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class KitCmd extends ConfigCommand {

    private final Kits plugin;

    public KitCmd(Kits plugin) {
        super("kit", "mostra ou resgata um kit", "/kit [nome]", new ArrayList<>(), "kit");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (strings.length == 0)
            return 0;

        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        final Player p = (Player) commandSender;

        // TODO: 9/29/20 Integrate with message API
        if (!plugin.getKitManager().canRescue(p.getUniqueId(), strings[0]))
            throw new SRCommandException(p, Message.KIT_UNAVAILABLE, plugin.getCore());

        return 1;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        final int syntax = validate(commandSender, s, strings);

        // TODO: 9/29/20 Integrate with message API
        if (syntax == 0) {
            if (commandSender instanceof Player)
                plugin.getKitsConfig().getKits().forEach(kit -> {
                    if (plugin.getKitManager().canRescue(((Player) commandSender).getUniqueId(), kit))
                        commandSender.sendMessage(kit);
                });
            else
                plugin.getKitsConfig().getKits().forEach(commandSender::sendMessage);

            return;
        }

        plugin.getKitManager().getKit(((Player) commandSender), strings[0]);
    }
}
